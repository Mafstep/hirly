package com.gitlab.mafstep.hirly.hirly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HirlyApplication {

    public static void main(String[] args) {
        SpringApplication.run(HirlyApplication.class, args);
    }

}
